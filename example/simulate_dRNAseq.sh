#!/bin/sh
python gensimreads_dRNAseq.py -n 50000000 -e NC_000915_genes_only.exlvl -l 100 --stranded --primary_enriched 50 NC_000915_genes_only.bed > NC_000915_genes_only_simulation_poisson.bed
perl bed2grp.pl NC_000915_genes_only_simulation_poisson.bed > NC_000915_genes_only_simulation_poisson.grp
