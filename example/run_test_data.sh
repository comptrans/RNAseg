#/bin/bash
../helper/rnaseg_parts.pl -p '-t 30 -k 10000 -c 1000 -w 100 -r 10 -R 10 -p 1.0 -a 0.5 -u 0.5 -f HP26695_fwd.grp -l 0.5' -l 1667867 -o 10000 -n 17 -s r10_a0.5_u0.5_fwd -f fwd_boundaries.csv
../helper/rnaseg_parts.pl -p '-t 30 -k 10000 -c 1000 -w 100 -r 10 -R 10 -p 1.0 -a 0.5 -u 0.5 -f HP26695_rev.grp --strand '-' -l 0.5' -l 1667867 -o 10000 -n 17 -s r10_a0.5_u0.5_rev -f rev_boundaries.csv
../helper/summarize_transcripts.pl -b fwd_boundaries.csv -c 0.3333 RNAseg_*.gff > r10_a0.5_u0.5_summarized.gff
./get_maxcp_gff.pl RNAseg_*_r10_a0.5_u0.5*.gff > r10_a0.5_u0.5_maxcp.gff
./merge_internal_neighbours.py r10_a0.5_u0.5_maxcp.gff NC_000915_GENES_ONLY.gff > r10_a0.5_u0.5_maxcp_merged.gff
