#!/usr/bin/perl

use strict;

my $in = shift;
my @starts;
my @cov;

open(IN,$in);

my $max_pos = 0;
my $c = 0;
while(<IN>) {
	$c++;	
	if ($c%1000 == 0) { 
		print STDERR "Processed $c lines of the input!\r";
	}

	my @a = split(/\s+/);

	#if ($a[4] =~ /\/1$/) {next;}

	if ($a[5] eq "-") {
		$starts[1][$a[2]]--;
		for (my $i = $a[1]; $i<=$a[2]; $i++) { $cov[1][$i]--; if ($i > $max_pos) {$max_pos = $i;}}
 	} else {
		$starts[0][$a[1]]++;
		for (my $i = $a[1]; $i<=$a[2]; $i++) { $cov[0][$i]++; if ($i > $max_pos) {$max_pos = $i;}}
	}

}

close(IN);

for (my $j=1; $j<=$max_pos; $j++) {

	if (not defined $starts[0][$j]) {$starts[0][$j] = 0;}
	if (not defined $starts[1][$j]) {$starts[1][$j] = 0;}
	if (not defined $cov[0][$j]) {$cov[0][$j] = 0;}
	if (not defined $cov[1][$j]) {$cov[1][$j] = 0;}
	print $starts[0][$j]."\t".$cov[0][$j]."\t".$starts[1][$j]."\t".$cov[1][$j]."\n";

}
exit;
