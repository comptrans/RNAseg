#! /usr/bin/python
import sys

def main():
    gff_pos_dict = read_gff_file(sys.argv[2]) #reads CDS.gff
    trans_dict, max_pos = read_trans_gff(sys.argv[1]) #reads maxcp.gff
    trans_dict = merge_trans_dict(trans_dict, gff_pos_dict) #merges neighbours and resolve overlaps
    print_trans_dict(trans_dict) #prints the final transcripts

def read_gff_file(cds_gff_file):
    """reads CDS from a gff file and creates a position based dictionary
    """
    gff_pos_dict = {'-':{}, '+':{}}
    with open(cds_gff_file) as cds_gff:
        for line in cds_gff:
            columns = line.split('\t')
            for position in xrange(int(columns[3]), int(columns[4])+1):
		gff_pos_dict[columns[6]][position] = True
    return gff_pos_dict

def read_trans_gff(trans_gff_file):
    """reads a RNAseg maxCP file into a dictionary
    """
    trans_dict = {'+':{}, '-':{}}
    max_pos = 0
    trans_count = 1
    count = 0
    with open(trans_gff_file) as trans_gff:
        for segment in trans_gff:
            columns = segment.split('\t')
            start = int(columns[3])
            end = int(columns[4])
            if columns[2] == 'transcript':
                if (start in trans_dict[columns[6]] and
                    not end in trans_dict[columns[6]][start]):
                    trans_dict[columns[6]][start].append(int(columns[4]))
                    count += 1
                else:
                    trans_dict[columns[6]][start] = [int(columns[4])]
                if int(columns[4]) > max_pos:
                    max_pos = int(columns[4])
    return trans_dict, max_pos

def merge_trans_dict(trans_dict, gff_pos_dict):
    """merges internal neighbours and resolves overlaps
    """
    new_trans_dict = {'+':{}, '-':{}}
    del_starts = {}
    for strand, starts in trans_dict.items():
        for start, ends in sorted(starts.items()):
            if not start in del_starts:
                new_ends = ends
                global_merged = 1
                while global_merged > 0:
                    new_ends, global_merged, del_starts = check_ends(starts, new_ends, del_starts, gff_pos_dict, strand)
                    ends += new_ends
                end = max(ends)
                for i in xrange(start, end+1):
                    if not i in new_trans_dict[strand]:
                        new_trans_dict[strand][i] = (start, end, end-start+1)
                    elif i in new_trans_dict[strand] and end-start > new_trans_dict[strand][i][2]:
                        new_trans_dict = clean(new_trans_dict, strand, new_trans_dict[strand][i])
                        new_trans_dict[strand][i] = (start, end, end-start+1)
                    else:
                        break
    return new_trans_dict

def check_ends(starts, ends, del_starts, gff_pos_dict, strand):
    """checks if end is neighbour of an internal start
    """
    new_ends = []
    global_merged = 0
    for end in ends:
        if end+1 in starts and end+1 in gff_pos_dict[strand]:
            global_merged += 1
            del_starts[end+1] = True
            new_ends += starts[end+1]
        else:
            merged = False
    return new_ends, global_merged, del_starts

def clean(new_trans_dict, strand, positions):
    """deletes shorter overlapping transcripts
    """
    for i in xrange(positions[0], positions[1]+1):
        new_trans_dict[strand].pop(i, None)
    return new_trans_dict

def print_trans_dict(trans_dict):
    """returns all final transcripts
    """
    for strand, positions in trans_dict.items():
        for transcript in list(set(positions.values())):
            print '\t'.join(['SeqID\tRNAseg\ttranscript',str(transcript[0]), str(transcript[1]),'.', strand, '.\tcolour=255 99 71'])
    
if __name__ == '__main__':
    main()

