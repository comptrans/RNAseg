#! /usr/bin/perl

# This script takes the result of a segmentation run with RNAseg as input and
# prints the segmentation with the largest change point number to stdout.

# Author: Bjoern Voss

my @res =();

while(<>) {

    if ($_ =~ /^(\d+) change points/) {
        @res = (); next;
    }
	
	push(@res,$_)

}

foreach (@res) {print;}

exit;
