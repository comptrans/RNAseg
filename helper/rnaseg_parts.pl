#! /usr/bin/perl

use strict;
use Getopt::Std;


my $usage = "perl rnaseg_parts.pl -p general_RNAseg_parameters -l length -n #parts -o overlap -s output_suffix -f filename for boundaries\nExample:\nrnaseg_parts.pl -p '-t 10 -k 10000 -c 1000 -r 100 -R 100 -a 10 -u 100 -f data.grp' -l 10000000 -n 50 -o 10000 -s e_coli_fwd -f fwd_bounds.csv\n";

my %opts;
getopts('hp:l:n:o:s:f:',\%opts);

if ($opts{'h'} || not ($opts{'p'} && $opts{'l'} && $opts{'n'} && $opts{'o'}  && $opts{'s'} && $opts{'f'})) {
    print $usage;
    exit;
}

my $paras = $opts{'p'};
my $length = $opts{'l'};
my $parts = $opts{'n'};
my $overlap = $opts{'o'};
my $suffix = $opts{'s'};
my $file = $opts{'f'};

my $l = int($length/$parts)+1;
my $o = int($overlap/2)+1;

open(BOUND, ">".$file);

for (my $i = 1; $i<=$length; $i+=$l) {
    my $start;
    my $end;
    
    if ($i ==1) {
        $start = 1;
    } else {
        $start = $i-$o;
    }
    if ($i+$l > $length) {
        $end = $length;
    } else {
        $end = $i + $l +$o;
    }

    print BOUND $start."\t".$end."\n";

    system("RNAseg -s $start -e $end --gff_file RNAseg_".$start."_".$end."_".$suffix.".gff --score_file RNAseg_".$start."_".$end."_".$suffix.".csv ".$paras."\n");
    
}

close(BOUND);

exit;
