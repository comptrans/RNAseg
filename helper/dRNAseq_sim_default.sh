#!/bin/bash

./genexplvprofile_dRNAseq.py $1 > $1.exlvl;
./gensimreads_dRNAseq.py -e $1.exlvl -n 1000000 --stranded $1 > $1.non_enriched.sim
./gensimreads_dRNAseq.py -e $1.exlvl -n 1000000 --stranded --primary_enriched 50 $1 > $1.enriched.sim
./bed2grp.pl $1.non_enriched.sim > $1.non_enriched.grp
./bed2grp.pl $1.enriched.sim > $1.enriched.grp

cut -f 1,2 $1.enriched.grp > $1.FWD.grp
cut -f 1,2 $1.non_enriched.grp >> $1.FWD.grp
cut -f 3,4 $1.enriched.grp > $1.REV.grp
cut -f 3,4 $1.non_enriched.grp >> $1.REV.grp

rm $1.non_enriched.grp
rm $1.enriched.grp

