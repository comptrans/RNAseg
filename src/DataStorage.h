/*
 * DataStorage.h
 *
 * Copyright (C) 2012 Björn Voß

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 * Authors: Thorsten Bischler and Björn Voß
 *
 */

#ifndef DATASTORAGE_H_
#define DATASTORAGE_H_

#include <string>
#include <vector>
#include <map>


class DataStorage {
private:
	std::vector<double> prim_read_starts;
	std::vector<double> sec_read_starts;
	std::vector<double> sec_read_coverage;
	std::vector<double> prim_read_coverage;
	std::vector<double> tata_p_value;

	void read_grp(std::string grp_file);


public:
	char strand;
	int start_pos, end_pos;
	DataStorage(char strand, std::string grp_file, int start_pos, int end_pos);
	virtual ~DataStorage();
	double get_prs(unsigned i);
	int get_max_prs_pos(int i, int j, double pvalue_cutoff, double read_cutoff2);
	double get_srs(unsigned i);
	double get_src(unsigned i);
	double get_prc(unsigned i);
	double get_sum_src(int i, int j);
	double get_pval(unsigned i);
	unsigned genome_size;
	unsigned fragment_size;
};

#endif /* DATASTORAGE_H_ */
