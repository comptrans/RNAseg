/*
 * DataStorage.cpp
 *
 * Copyright (C) 2012 Björn Voß

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 * Authors: Thorsten Bischler and Björn Voß
 *
 */

#include "DataStorage.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <ctype.h>
#include <cmath>

using namespace std;

DataStorage::DataStorage(char strand, string grp_file, int start_pos, int end_pos):
    strand(strand), start_pos(start_pos), end_pos(end_pos)
{
	read_grp(grp_file);
}

DataStorage::~DataStorage() {

}


void DataStorage::read_grp(string grp_file) {

	ifstream file(grp_file.c_str());
	string line;
	string count;
	vector<double> row;
	int pos_counter = 1;


	if(file.is_open()) {
		while(getline(file, line)) {
			if (((start_pos == 1) && (end_pos == 0)) || ((pos_counter >= start_pos) && (pos_counter <= end_pos))) {
				stringstream linestream(line);

				row.clear();
				while(getline(linestream, count, '\t')) {
					row.push_back(atof(count.c_str()));
				}
				prim_read_starts.push_back(row[0]);				
				prim_read_coverage.push_back(row[1]);
				sec_read_starts.push_back(row[2]);
				sec_read_coverage.push_back(row[3]);
				tata_p_value.push_back(row[4]);
			}
		pos_counter++;
		}
		file.close();
	}
	else {
		cerr << "Unable to open grp file for reading" << endl;
		exit(1);
	}
	fragment_size = sec_read_coverage.size();
    if (strand == '-' && end_pos == 0) {
        end_pos = start_pos + fragment_size - 1;
    }
    genome_size = pos_counter;
}


double DataStorage::get_prs(unsigned i) {
	if(strand == '+') {
		return prim_read_starts[i];
	}
	else {
		return abs(prim_read_starts[fragment_size-i-1]);
	}
}

double DataStorage::get_prc(unsigned i) {
	if(strand == '+') {
		return prim_read_coverage[i];
	}
	else {
		return abs(prim_read_coverage[fragment_size-i-1]);
	}
}

double DataStorage::get_pval(unsigned i) {
	if(strand == '+') {
		return tata_p_value[i];
	}
	else {
		return abs(tata_p_value[fragment_size-i-1]);
	}
}

int DataStorage::get_max_prs_pos(int i, int j, double pvalue_cutoff, double read_cutoff2) {
	int max_k, max_reads = -1;
	if (i < 0) {
		i = 0;
	}
	for (int k = i; k <= j; k++) {
	    bool pval_met = false;
	    for (int s = k - 13; s <= k - 9; s++) {
		if (s >= 0 && get_pval(s) <= pvalue_cutoff) { pval_met = true; }
	    }

	    if (pval_met && get_prs(k) >= max_reads) {
		max_k = k;
		max_reads = get_prs(k);
	    }
	}
	return max_k;
}

double DataStorage::get_srs(unsigned i) {
	if(strand == '+') {
		return sec_read_starts[i];
	}
	else {
		return abs(sec_read_starts[fragment_size-i-1]);
	}
}

double DataStorage::get_src(unsigned i) {
	if(strand == '+') {
		return sec_read_coverage[i];
	}
	else {
		return abs(sec_read_coverage[fragment_size-i-1]);
	}
}

double DataStorage::get_sum_src(int i, int j) {
	int sum = 0;

	for (int k = i; k <= j; k++) {
		sum += get_src(k);
	}
	return sum;
}


