/*
 * Segmentation.h
 *
 * Copyright (C) 2012 Björn Voß

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

 * Authors: Thorsten Bischler and Björn Voß
 *
 */

#ifndef SEGMENTATION_H_
#define SEGMENTATION_H_

#include <string>
#include <fstream>
#include <map>
#include "DataStorage.h"

struct SegBound {
	int start;
	int end;

	SegBound(int start = -1, int end = -1) : start(start), end(end) {}

	friend bool operator<(const SegBound& lhs, const SegBound& rhs) {
		return lhs.start < rhs.start;
	}
};

class Segmentation {

private:
	DataStorage data;
	int n;
	int genome_size;
	int kmax;
	int cpmax;
    int zmax;
    int zfrac;
	int prim_window;
	double read_cutoff;
	double read_cutoff2;
	double pvalue_cutoff;
	double mean_cov_cutoff;
	std::string gff_file, score_file;
	int kmin;
    bool verbose;
    double ratio;
    double non_transcript_cov_cutoff;
	std::map<int, int> cp_count;
	std::vector<SegBound> trans_list;
	int step_width;



	void compute_G(std::vector<std::vector<double> >& G);
	void compute_N(std::vector<int >& N);
	void writeTransToFile(int start, int end, std::ofstream &outfile);
	void writeNonTransToFile(int start, int end, std::ofstream &outfile);
	void writeOutputFile(std::vector<int >& N, std::vector<std::vector <int> >& th, std::string gff_file, std::string score_file);
	int conv(int i);
	//void writeOutputFile_merged(std::vector<int >& N, std::vector<std::vector <int> >& th, std::string gff_file);


public:

	Segmentation(DataStorage data, int kmax, int cpmax, int prim_window, double read_cutoff, double read_cutoff2, double pvalue_cutoff, double mean_cov_cutoff, std::string gff_file, std::string score_file, int kmin, bool verbose, double ratio, double non_transcript_cov_cutoff, int zmax, int zfrac, int step_width);

	void computeSegments();

};


#endif /* SEGMENTATION_H_ */
